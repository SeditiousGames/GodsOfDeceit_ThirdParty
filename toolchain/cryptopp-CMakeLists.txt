#  (The MIT License)
#
#  Copyright (c) 2018 - 2019 Mamadou Babaei
#  Copyright (c) 2018 - 2019 Seditious Games Studio
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.


CMAKE_MINIMUM_REQUIRED ( VERSION 2.8 )

PROJECT ( cryptopp )

FIND_PACKAGE ( Threads REQUIRED )

SET ( CMAKE_CXX_EXTENSIONS          OFF )
SET ( CMAKE_CXX_STANDARD            17 )
SET ( CMAKE_CXX_STANDARD_REQUIRED   ON )
SET ( CMAKE_THREAD_PREFER_PTHREAD   TRUE )
SET ( THREADS_PREFER_PTHREAD_FLAG   TRUE )

# WORKAROUND
# -DCRYPTOPP_DISABLE_ASM
# error: always_inline function '_mm_shuffle_epi8' requires target feature
# 'ssse3', but would be inlined into function 'ARIA_ProcessAndXorBlock_SSSE3'
# that is compiled without

SET ( GOD_COMMON_FLAGS  "-DCRYPTOPP_DISABLE_ASM" )

SET ( CMAKE_C_FLAGS     "${GOD_COMMON_FLAGS}" )
SET ( CMAKE_CXX_FLAGS   "${GOD_COMMON_FLAGS}" )

FILE ( GLOB SOURCE_FILES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "*.cpp" "*.h" )

ADD_LIBRARY ( ${CMAKE_PROJECT_NAME} STATIC ${SOURCE_FILES} )

TARGET_LINK_LIBRARIES ( ${CMAKE_PROJECT_NAME} Threads::Threads )
