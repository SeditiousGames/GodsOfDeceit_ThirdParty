#  (The MIT License)
#
#  Copyright (c) 2018 - 2019 Mamadou Babaei
#  Copyright (c) 2018 - 2019 Seditious Games Studio
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.


SET ( GOD_ARCHITECTURE_TRIPLE                   "$ENV{GOD_ARCHITECTURE_TRIPLE}" )
SET ( GOD_TOOLCHAIN_BIN_DIRECTORY               "$ENV{GOD_TOOLCHAIN_BIN_DIRECTORY}" )
SET ( GOD_CLANG_INCLUDE_DIRECTORY               "$ENV{GOD_CLANG_INCLUDE_DIRECTORY}" )
SET ( GOD_TOOLCHAIN_COMPILER_INCLUDE_DIRECTORY  "$ENV{GOD_TOOLCHAIN_COMPILER_INCLUDE_DIRECTORY}" )
SET ( GOD_TOOLCHAIN_LIBCPP_INCLUDE_DIRECTORY    "$ENV{GOD_TOOLCHAIN_LIBCPP_INCLUDE_DIRECTORY}" )
SET ( GOD_TOOLCHAIN_SYSTEM_INCLUDE_DIRECTORY    "$ENV{GOD_TOOLCHAIN_SYSTEM_INCLUDE_DIRECTORY}" )

SET ( CMAKE_CXX_EXTENSIONS                  OFF )
SET ( CMAKE_CXX_STANDARD                    14 )
SET ( CMAKE_CXX_STANDARD_REQUIRED           OFF )
SET ( CMAKE_INTERPROCEDURAL_OPTIMIZATION    FALSE )
SET ( CMAKE_POSITION_INDEPENDENT_CODE       TRUE )

SET ( GOD_COMMON_COMPILER_FLAGS         "-m64 -pipe" )
SET ( GOD_COMMON_COMPILER_FLAGS_DEBUG   "${GOD_COMMON_COMPILER_FLAGS}" )
SET ( GOD_COMMON_COMPILER_FLAGS_RELEASE "${GOD_COMMON_COMPILER_FLAGS} -fdata-sections -ffunction-sections -fomit-frame-pointer" )
SET ( GOD_COMMON_C_FLAGS                "-nostdinc -isystem\"${GOD_TOOLCHAIN_COMPILER_INCLUDE_DIRECTORY}\" -isystem\"${GOD_TOOLCHAIN_SYSTEM_INCLUDE_DIRECTORY}\"" )
SET ( GOD_COMMON_C_FLAGS_DEBUG          "${GOD_COMMON_COMPILER_FLAGS_DEBUG} ${GOD_COMMON_C_FLAGS}" )
SET ( GOD_COMMON_C_FLAGS_RELEASE        "${GOD_COMMON_COMPILER_FLAGS_RELEASE} ${GOD_COMMON_C_FLAGS}" )
SET ( GOD_COMMON_CXX_FLAGS              "-fcxx-exceptions -fexceptions -frtti -nostdinc -nostdinc++ -isystem\"${GOD_TOOLCHAIN_LIBCPP_INCLUDE_DIRECTORY}\" -isystem\"${GOD_TOOLCHAIN_COMPILER_INCLUDE_DIRECTORY}\" -isystem\"${GOD_TOOLCHAIN_SYSTEM_INCLUDE_DIRECTORY}\"" )
SET ( GOD_COMMON_CXX_FLAGS_DEBUG        "${GOD_COMMON_COMPILER_FLAGS_DEBUG} ${GOD_COMMON_CXX_FLAGS}" )
SET ( GOD_COMMON_CXX_FLAGS_RELEASE      "${GOD_COMMON_COMPILER_FLAGS_RELEASE} ${GOD_COMMON_CXX_FLAGS}" )
SET ( GOD_COMMON_LINKER_FLAGS           "-fuse-ld=lld -nodefaultlibs -lc -lc++ -lc++abi -lm -m64 -stdlib=libc++" )
SET ( GOD_COMMON_LINKER_FLAGS_DEBUG     "${GOD_COMMON_LINKER_FLAGS}" )
SET ( GOD_COMMON_LINKER_FLAGS_RELEASE   "${GOD_COMMON_LINKER_FLAGS} -Wl,--gc-sections -Wl,--strip-all" )

SET ( CMAKE_C_FLAGS_DEBUG_INIT                  "${GOD_COMMON_C_FLAGS_DEBUG}" )
SET ( CMAKE_C_FLAGS_RELEASE_INIT                "${GOD_COMMON_C_FLAGS_RELEASE}" )
SET ( CMAKE_CXX_FLAGS_DEBUG_INIT                "${GOD_COMMON_CXX_FLAGS_DEBUG}" )
SET ( CMAKE_CXX_FLAGS_RELEASE_INIT              "${GOD_COMMON_CXX_FLAGS_RELEASE}" )
SET ( CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT         "${GOD_COMMON_LINKER_FLAGS_DEBUG}" )
SET ( CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT       "${GOD_COMMON_LINKER_FLAGS_RELEASE}" )
SET ( CMAKE_MODULE_LINKER_FLAGS_DEBUG_INIT      "${GOD_COMMON_LINKER_FLAGS_DEBUG}" )
SET ( CMAKE_MODULE_LINKER_FLAGS_RELEASE_INIT    "${GOD_COMMON_LINKER_FLAGS_RELEASE}" )
SET ( CMAKE_SHARED_LINKER_FLAGS_DEBUG_INIT      "${GOD_COMMON_LINKER_FLAGS_DEBUG}" )
SET ( CMAKE_SHARED_LINKER_FLAGS_RELEASE_INIT    "${GOD_COMMON_LINKER_FLAGS_RELEASE}" )

SET ( CMAKE_CROSSCOMPILING          TRUE )
SET ( CMAKE_SYSTEM_NAME             Linux )
SET ( CMAKE_SYSTEM_VERSION          0 )
SET ( CMAKE_LIBRARY_ARCHITECTURE    "${GOD_ARCHITECTURE_TRIPLE}" )
SET ( CMAKE_C_COMPILER_TARGET       "${GOD_ARCHITECTURE_TRIPLE}" )
SET ( CMAKE_CXX_COMPILER_TARGET     "${GOD_ARCHITECTURE_TRIPLE}" )

SET ( CMAKE_SYSROOT "${GOD_TOOLCHAIN_ROOT_DIRECTORY}" )

SET ( CMAKE_FIND_ROOT_PATH              "${GOD_TOOLCHAIN_ROOT_DIRECTORY}" )
SET ( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
SET ( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
SET ( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )
SET ( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )

# Due to LTO optimization being enabled,
# first prefer the LLVM Archiver,
# if not then GCC Archiver,
# otherwise the Binutils Archiver
IF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/llvm-ar" )
    # LLVM Archiver
    SET ( CMAKE_AR              "${GOD_TOOLCHAIN_BIN_DIRECTORY}/llvm-ar" )
ELSEIF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-gcc-ar" )
    # GCC Archiver
    SET ( CMAKE_AR              "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-gcc-ar" )
ELSE (  )
    # Binutils Archiver
    SET ( CMAKE_AR              "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-ar" )
ENDIF (  )

SET ( CMAKE_ASM_COMPILER        "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-as" )

IF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/clang" )
    SET ( CMAKE_C_COMPILER      "${GOD_TOOLCHAIN_BIN_DIRECTORY}/clang" )
ELSEIF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-gcc" )
    SET ( CMAKE_C_COMPILER      "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-gcc" )
ELSE (  )
    SET ( CMAKE_C_COMPILER  "   ${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-cc" )
ENDIF (  )

SET ( CMAKE_RANLIB              "/bin/true" )

SET ( CMAKE_C_COMPILER_AR       "${CMAKE_AR}" )
SET ( CMAKE_C_COMPILER_RANLIB   "${CMAKE_RANLIB}" )
SET ( CMAKE_C_LINK_EXECUTABLE   "<CMAKE_LINKER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>" )
SET ( CMAKE_C_PREPROCESSOR      "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-cpp" )

IF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/clang++" )
    SET ( CMAKE_CXX_COMPILER    "${GOD_TOOLCHAIN_BIN_DIRECTORY}/clang++" )
ELSEIF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-g++" )
    SET ( CMAKE_CXX_COMPILER    "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-g++" )
ELSE (  )
    SET ( CMAKE_CXX_COMPILER    "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-c++" )
ENDIF (  )

SET ( CMAKE_CXX_COMPILER_AR     "${CMAKE_AR}" )
SET ( CMAKE_CXX_COMPILER_RANLIB "${CMAKE_RANLIB}" )
SET ( CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_LINKER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>" )

SET ( CMAKE_LINKER              "${CMAKE_CXX_COMPILER}" )

# Prefer the Binutils nm over the GCC nm
IF ( EXISTS "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-nm" )
    SET ( CMAKE_NM              "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-nm" )
ELSE (  )
    SET ( CMAKE_NM              "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-gcc-nm" )
ENDIF (  )

SET ( CMAKE_OBJCOPY             "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-objcopy" )
SET ( CMAKE_OBJDUMP             "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-objdump" )
SET ( CMAKE_STRIP               "${GOD_TOOLCHAIN_BIN_DIRECTORY}/${GOD_ARCHITECTURE_TRIPLE}-strip" )

MESSAGE ( STATUS "Is CMake cross compiling? ${CMAKE_CROSSCOMPILING}" )
